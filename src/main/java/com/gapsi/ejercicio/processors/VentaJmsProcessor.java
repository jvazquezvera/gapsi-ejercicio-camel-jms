package com.gapsi.ejercicio.processors;


import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.gapsi.ejercicio.dto.VentaDto;
import com.gapsi.ejercicio.routers.JMSRoutes;


@Component("ventaJmsProcessor")
public class VentaJmsProcessor implements Processor {
	
	private static final String PIPE_SEPARATOR="\\|";
	private static final String REPLACE_REGEXP="(\n|\r)";
	private static final String EMPTY_STRING = "";
	private static final String NAME_LIST_TO_INSERT = "torest";
	
	
	
	static final Logger log = LoggerFactory.getLogger(JMSRoutes.class);
    
	public void process(Exchange exchange) throws Exception {
		 try {
	  	    	
	  	    	
		        String[] convertedMessage = exchange.getMessage().getBody(String.class)
						 .replaceAll(REPLACE_REGEXP, EMPTY_STRING).split(PIPE_SEPARATOR);
				 
	             VentaDto ventadto =new VentaDto();
	             ventadto.setFecha(convertedMessage[0].substring(0,8));
	             ventadto.setMonto(new BigDecimal(convertedMessage[1]));
	             ventadto.setSucursal(Short.valueOf(convertedMessage[2]));
	             ventadto.setCliente(convertedMessage[3]);
	             ventadto.setCanal(convertedMessage[4]);
	             
	             @SuppressWarnings("unchecked")
			   	 ArrayList<VentaDto> toBody=(ArrayList<VentaDto>) exchange.getProperty(NAME_LIST_TO_INSERT);
	             toBody.add(ventadto);
	  	     }catch(Exception e) {
	  	    	log.error("el registro no es correcto"+exchange.getMessage().getBody(String.class));
	  	     }
    }

	
}