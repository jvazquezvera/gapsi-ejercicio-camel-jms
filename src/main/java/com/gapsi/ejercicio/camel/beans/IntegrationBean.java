package com.gapsi.ejercicio.camel.beans;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;


@Component("integrationBean")
public class IntegrationBean {
	
	
	public void setHeadersRestOp(Exchange exchange) {
		exchange.getMessage().setHeader(Exchange.CONTENT_TYPE, "aplication/json");
		exchange.getMessage().setHeader(Exchange.HTTP_METHOD, "POST");
		exchange.getMessage().removeHeader(Exchange.HTTP_URI);
	}
	
}
