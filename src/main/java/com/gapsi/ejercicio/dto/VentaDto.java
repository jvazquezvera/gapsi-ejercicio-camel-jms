package com.gapsi.ejercicio.dto;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VentaDto {

	@JsonProperty("fecha")
	private String fecha;
	@JsonProperty("monto")
	private BigDecimal monto;
	@JsonProperty("sucursal")
	private Short sucursal;
	@JsonProperty("cliente")
	private String cliente;
	@JsonProperty("canal")
	private String canal;
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Short getSucursal() {
		return sucursal;
	}
	public void setSucursal(Short sucursal) {
		this.sucursal = sucursal;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	@Override
	public String toString() {
		return "VentaDto [fecha=" + fecha + ", idGaleria=" + monto + ", sucursal=" + sucursal + ", cliente="
				+ cliente + ", canal=" + canal + "]";
	}
	
	
	
	
}
