package com.gapsi.ejercicio.routers;


import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gapsi.ejercicio.dto.VentaDto;
import com.gapsi.ejercicio.processors.VentaJmsProcessor;

@Component
public class JMSRoutes extends RouteBuilder {
	static final Logger log = LoggerFactory.getLogger(JMSRoutes.class);
	
	private static final String NAME_LIST_TO_INSERT = "torest";

	@Autowired
	VentaJmsProcessor ventaJmsProcessor;
	
	@Override
	public void configure() throws Exception {
		  
		
		from("activemq:queue:{{input.queue}}").setProperty(NAME_LIST_TO_INSERT, constant(new ArrayList<VentaDto>()))
		   		.split(bodyAs(String.class).tokenize("\n"))
		   			.process(ventaJmsProcessor)
		   		.end()
		   .setBody(simple("${exchangeProperty.torest}"))
		   .bean("integrationBean","setHeadersRestOp")
		   .marshal().json(JsonLibrary.Jackson)
		   .to("http://localhost:8080/api/ventas/importacion")
		   .log(LoggingLevel.INFO, log,"insertados: ${body}");
		   
	}

}
