package com.gapsi.ejercicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class GapsiEjercicioJMSApplication {

	
	@Autowired
    ApplicationContext aplicationContext;
	
	public static void main(String[] args) {
		SpringApplication.run(GapsiEjercicioJMSApplication.class, args);
	}
	
	
	
	  
	

}
